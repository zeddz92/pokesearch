//
//  Pokemon.swift
//  pokeSearch
//
//  Created by Edward R on 3/9/17.
//  Copyright © 2017 Edward R. All rights reserved.
//

import Foundation

class Pokemon {
    private var _id: Int!
    private var _name: String!
    
    var id: Int {
        get {
            return _id
        }
    }
    
    var name: String {
        get {
            return _name
        }
    }
    
    init(id: Int, name: String) {
        _id = id
        _name = name
    }
}
