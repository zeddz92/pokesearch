//
//  PokeCell.swift
//  pokedex
//
//  Created by Edward R on 3/4/17.
//  Copyright © 2017 Edward R. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
  
    var pokemon: Pokemon!
    
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 5.0
    }
    
    func configureCell(pokemon: Pokemon) {
        self.pokemon = pokemon
        nameLbl.text = self.pokemon.name.capitalized
        thumbImage.image = UIImage(named: "\(self.pokemon.id)")
    }
    
    
    
}


