//
//  DataBetweenControllers.swift
//  pokeSearch
//
//  Created by Edward R on 3/9/17.
//  Copyright © 2017 Edward R. All rights reserved.
//

import Foundation

protocol RetrievePokemonDelegate {
    func retrievedPokemon(pokemon: Pokemon)
}
